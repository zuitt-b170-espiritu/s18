/*
	log in the console the following grades of a student
	98.5
	94.3
	89.2
	90.0
*/

let grades = [98.5, 94.3, 89.2, 90.0];

console.log(grades);
console.log(grades[3]);


/*
Objects
	collection of related data and functionalities; usually representing real-world objects

*/
let grade = {
	//curly braces - initializer for creating objects
	// key-value pair
		/*
			key - field/identifier/property to describe the data/value
			value - information that serves as the value of the key/field

		*/
	math:98.5,
	english:94.3,
	science:90.0,
	MAPEH:89.2
};
console.log(grade);

/*
ACCESSING ELEMENTS INSIDE THE OBJECT
	SYNTAX FOR CREATING AN OBJECT
		let objectName = {
			keyA: valueA,
			keyA: valueB,
			keyA: valueC,

		}

	Dot Notation - used to access the specific property and value of the object: prefered in terms of object access
*/
console.log(grade.english);
// accessing two keys in the object
console.log(grade.english, grade.math);

/*
create a cellphone object that has the following keys(supply your own values for each)

	brand
	color
	mfd
	take a screenshot of your output and send it to our gc
*/


let phone = {
	Brand:"Samsung",
	Color:"gray",
	MFD:2022, 
};
console.log(phone);
// typeof determines the data type of the element
console.log(typeof phone);


let student = {
	firstName:"John",
	lastName:"Smith",
	mobileNo:09521452875,
	location:{
		city:"Tokyo",
		country:"Japan"
	},
	email:["john@mail.com", "john.smith@mail.com"],

	fullName:(function name(fullName){
		// this - refers to the object where it is inserted
		console.log(this.firstName + " " + this.lastName)
	})
};
// console.log(student.firstName + "" + student.lastName);
student.fullName();
console.log(student.location.city);
console.log(student.email);
console.log(student.email[0]);


/*
ARRAY OF OBJECTS
*/


let contactList = [
{
	firstName:"John",
	lastName:"Smith",
	location:"Japan",
},
{
	firstName:"Jane",
	lastName:"Smith",
	location:"Japan",
},
{
	firstName:"Jasmine",
	lastName:"Smith",
	location:"Japan",
}
];
// accessing an element inside the array
console.log(contactList[1]);
// accessing a key in an object that is inside the array
console.log(contactList[2].firstName);




/*
Constructor Function - use to create reusable function to create several objects
					- useful for creating copies/instances of an object

	SYNTAX:
		function objectName (keyA, keyB){
			this.keyA = keyA,
			this.keyB = keyB,
		}
*/

/*
object literals - let objectName = {}

instance - concrete occurence of any object which emphasizes on the unique identity of it

*/


// Reusable Function Object method
function Laptop(name, manufacturedDate){
	this.name=name,
	this.manufacturedDate=manufacturedDate

	/*
	it also performs different commands 
	console.log(this);
	*/
};

// new keyword refers/signifies that there will be a new object unnder the Laptop function
let laptop1 = new Laptop("Lenovo", 2008);
console.log(laptop1);


let laptop2 = new Laptop("Toshiba", 1997);
console.log(laptop2);


/*
create a variable and store the laptop1 and laptop2 in it (the resulting output must be an array)
log the array in the console

*/
let array = [laptop1, laptop2];
console.log(array);


/*
initializing, adding, deleting, reaassigning of object properties

*/
let car = {};
console.log(car);
// adding object properties

car.name = "Honda Civic";
console.log(car);


car["manufacturedDate"] = 2020;
console.log(car);


// reassigning object (same/existing properties, but assigning a different value)
// car["name"] = "volvo";
car.name = "volvo";
console.log(car);


// deleting object properties

// delete car.manufacturedDate;
// console.log(car);


delete car['manufacturedDate'];
console.log(car);



let person = {
	name: "John",
	talk: function(){
		console.log('Hi! My name is' + this.name)
	}
}


// add property
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}



let friend = {
	firstName: "Joe",
	lastName: "Doe",
	address:{
		city: "Austin",
		state: "Texas"
	},
	emails:["joe@mail.com", "joedoe@mail.com"],
	introduce: function(){
		console.log("Hello! My name is "+ this.firstName + " " + this.lastName)
	}
};


let pokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	// tackle: function (target){
	// 	// console.log("This pokemon tackled targetPokemon")
	// 	console.log(this.name + " tackled " + target.name);
	// 	console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	// }
}

function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level
	// Skills of Pokemon
	pokemon.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	}
	pokemon.faint = function(target){
		console.log(this.name + " fainted.")
	}
}
let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 8);
let Balbasaur = new Pokemon("Balbasaur", 9);
let Raichu = new Pokemon("Raichu", 16);
