let trainor = {
	firstName: "Ash",
	age: 12,
	pokemons:["Pikachu", "Squirtle", "Snorlax", "Dragonite", "Gible"], 
	talk: function(){
		console.log("Hey there! I am "+ this.firstName);
		console.log("Playing Pokemon theme song");
		console.log("I want to be the very best Like no one ever was");
		console.log("To catch them is my real test To train them is my cause");
		console.log("Pikachu I choose you!");
	}
};


let enemy = {
	firstName: "Morrison",
	age: 12,
	pokemons:["Girafarig", "Growlithe", "Swampert", "Steelix", "Gligar"],
	talk: function(){
		console.log("Let us battle "+ trainor.firstName + " to see whos pokemon is stronger");
		console.log("Steelix I choose you!");
	}
};

enemy.rebutt = function(){
	console.log("Is that all you've got " + trainor.firstName + " you have a long way to go");
};


let pokemon = {
	name: "Pikachu",
	level: 7,
	health: 100,
	attack: 40,
	// tackle: function (Pokemonstat){
	// 	console.log("This pokemon tackled targetPokemon")
	// 	console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	// // damage = Pokemonstat.health() - this.attack;
	// 	// console.log("Enemy's pokemon health is reduced to " + damage);
	// }
};


function Pokemonstat(name, level, health, attack){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level,
	// Skills of Pokemon
	pokemon.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		// console.log("targetPokemon's health is now reduced to targetPokemonHealth");
		let damage = (target.health - this.attack);
		console.log(target.name + " health is reduced to " + damage);
		if (damage <= 0) {
			pokemon.faint();
		}
		else{
			enemy.rebutt();
		}

	}
	pokemon.faint = function(target){
		console.log("Enemy's pokemon had taken serious damage and fainted.")

	}
}
let Swampert = new Pokemonstat ("Swampert", 16, 100, 50);
let Steelix = new Pokemonstat("Steelix", 30, 100, 30);
let Girafarig = new Pokemonstat("Girafarig", 8, 100, 30);
let Growlithe = new Pokemonstat("Growlithe", 10, 100, 30);
let Gligar = new Pokemonstat("Gligar", 15, 100, 30);